const fs = require("fs")
const BufferedWriter = require("./bufferedWriter");
if (process.argv[2]) {
    class GridChecker {
        constructor() { }
        _setup(grid) {
            this.grid = grid;
            this.rowNum = grid.length;
            this.colNum = grid[0].length;
            this.currentRow = 0;
        }
        _isValidPosition(row, col) {
            return row >= this.currentRow && row < this.rowNum && col >= 0 && col < this.colNum;
        }
        _findGroup(firstRow, firstCol) {
            var queue = [[firstRow, firstCol]];
            var group = [];
            while (queue.length) {
                var point = queue.shift();
                var row = point[0];
                var col = point[1];
                if (this.grid[row][col] == 1) {
                    var start = col
                    var end = col
                    var valid = true;
                    while (valid) {
                        if (this._isValidPosition(row, start - 1) && this.grid[row][start - 1] == 1) {
                            start--;
                        } else {
                            valid = false;
                        }
                    }
                    valid = true;
                    while (valid) {
                        if (this._isValidPosition(row, end + 1) && this.grid[row][end + 1] == 1) {
                            end++;
                        } else {
                            valid = false;
                        }
                    }
                    for (var i = start; i <= end; i++) {
                        group.push([row, i]);
                        this.grid[row][i] = 0;
                        if (this._isValidPosition(row + 1, i) && this.grid[row + 1][i] == 1) {
                            queue.push([row + 1, i]);
                        }
                        if (this._isValidPosition(row - 1, i) && this.grid[row - 1][i] == 1) {
                            queue.push([row - 1, i]);
                        }
                    }
                }
            }
            return group;
        }
        findAllGroups(grid) {
            this._setup(grid);
            var result = [];
            while (this.currentRow < this.rowNum) {
                for (var col = 0; col < this.colNum; col++) {
                    if (this.grid[this.currentRow][col] == 1) {
                        var group = this._findGroup(this.currentRow, col);
                        if (group.length > 1) {
                            result.push(group);
                        }
                    }
                }
                //Removes already processed rows to free up memory
                delete this.grid[this.currentRow]
                this.currentRow++;
                console.log(this.currentRow)
            }
            return result;
        }
    }

    function writeResult(file, result) {
        var bw = new BufferedWriter(file, 100);
        bw.write("[")
        bw.write(JSON.stringify(result[0]));
        for (var i = 1; i < result.length; i++) {
            bw.write("," + JSON.stringify(result[i]))
        }
        bw.write("]")
        bw.end();
    }

    try {
        var grid = JSON.parse(fs.readFileSync(process.argv[2]).toString());
    } catch (e) {
        console.warn("Invalid json file.")
        process.exit()
    }
    var checker = new GridChecker();
    var result = checker.findAllGroups(grid);
    writeResult("result.json", result);
    console.log("Found " + result.length + " groups");

} else {
    console.log("No json given")
}