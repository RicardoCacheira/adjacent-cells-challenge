/*
The Buffered writer was created to write multiple times to the same file without overloading the file I/O.
*/

const fs = require("fs");
class BufferedWriter {
    constructor(file, bufferLength = 1) {
        this.file = file
        this.bufferLength = bufferLength * 1024 * 1024
        this.buffer = "";
        fs.writeFileSync(file, "");
    }
    _appendBuffer() {
        fs.appendFileSync(this.file, this.buffer);
        this.buffer = "";
    }
    write(content) {
        if (this.buffer.length + content.length > this.bufferLength) {
            var diff = this.bufferLength - this.buffer.length
            this.buffer += content.substring(0, diff)
            this._appendBuffer();
            this.write(content.substring(diff));
        } else {
            this.buffer += content
        }
    }
    end() {
        this._appendBuffer();
    }
}

module.exports=BufferedWriter