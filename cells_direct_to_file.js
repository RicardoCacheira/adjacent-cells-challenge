const fs = require("fs");
const BufferedWriter = require("./bufferedWriter");
if (process.argv[2]) {
    class GridChecker {
        constructor() { }
        _setup(grid, file) {
            this.grid = grid;
            this.rowNum = grid.length;
            this.colNum = grid[0].length;
            this.currentRow = 0;
            this.groupNum = 0;
            this.writer = new BufferedWriter(file, 100);
            this.writer.write("[")
        }
        _isValidPosition(row, col) {
            return row >= this.currentRow && row < this.rowNum && col >= 0 && col < this.colNum;
        }
        _findGroup(firstRow, firstCol) {
            var queue = [[firstRow, firstCol]];
            var group = [];
            while (queue.length) {
                var point = queue.shift();
                var row = point[0];
                var col = point[1];
                if (this.grid[row][col] == 1) {
                    var start = col
                    var end = col
                    var valid = true;
                    while (valid) {
                        if (this._isValidPosition(row, start - 1) && this.grid[row][start - 1] == 1) {
                            start--;
                        } else {
                            valid = false;
                        }
                    }
                    valid = true;
                    while (valid) {
                        if (this._isValidPosition(row, end + 1) && this.grid[row][end + 1] == 1) {
                            end++;
                        } else {
                            valid = false;
                        }
                    }
                    for (var i = start; i <= end; i++) {
                        group.push([row, i]);
                        this.grid[row][i] = 0;
                        if (this._isValidPosition(row + 1, i) && this.grid[row + 1][i] == 1) {
                            queue.push([row + 1, i]);
                        }
                        if (this._isValidPosition(row - 1, i) && this.grid[row - 1][i] == 1) {
                            queue.push([row - 1, i]);
                        }
                    }
                }
            }
            return group;
        }
        findAllGroups(grid, file) {
            this._setup(grid, file);
            while (this.currentRow < this.rowNum) {
                for (var col = 0; col < this.colNum; col++) {
                    if (this.grid[this.currentRow][col] == 1) {
                        var group = this._findGroup(this.currentRow, col);
                        if (group.length > 1) {
                            if (this.groupNum > 0) {
                                this.writer.write("," + JSON.stringify(group))
                            } else {
                                this.writer.write(JSON.stringify(group))
                            }
                            this.groupNum++;
                        }
                    }
                }
                //Removes already processed rows to free up memory
                delete this.grid[this.currentRow]
                this.currentRow++;
                console.log(this.currentRow)
            }
            this.writer.write("]")
            this.writer.end();
        }
    }

    try {
        var grid = JSON.parse(fs.readFileSync(process.argv[2]).toString());
    } catch (e) {
        console.warn("Invalid json file.")
        process.exit()
    }
    var checker = new GridChecker("result.json");
    checker.findAllGroups(grid, "result.json");
    console.log("Found " + checker.groupNum + " groups");
} else {
    console.log("No json given")
}