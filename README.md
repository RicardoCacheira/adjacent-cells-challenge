# Adjacent Cells Challenge

Neste repositório encontram-se duas possiveis resoluções do desafio apresentado considerando certas restrições.
Para todas as resoluções foi utilizada a linguagem JavaScript a correr em Node v10.15.3.

1. `cells_result_in_memory`

    Nesta resolução é considerado que não é necessário manter a grelha em memória, podendo assim ser modificada durante o processo de identificação dos grupos, no entanto é considerado que o resultado deva ser mantido.

    Isto pode ser necessário caso se queira fazer alguma análise dos resultados após serem gerados no mesmo processo.

    Nota: Nesta versão foi utilizado o `BufferedWriter` para escrever o resultado apesar do resultado ser escrito todo duma só vez dado que ao utilizar o `JSON.parse` para tornar o resultado no JSON desejado criava uma string de tamanho invalido no exemplo `20000x20000`.

2. `cells_direct_to_file`

    Nesta resolução é considerado que o único objectivo seja a geração do ficheiro correspondente aos resultados.
    Dado a não necessitar manter o resultado em memória é consideravelmente mais leve a nivel de memória do que a outra opção.

# Resultados

Aqui encontram-se os resultados das resoluções para os exemplos fornecidos.

Todos os testes apresentados foram corridos num Macbook Pro 15 polegadas de 2015 com um Intel Core i7 a 2.5Ghz e com 16GB de ram a correr macOS Mojave versão 10.14.3.

## Tempo

Os resultados apresentados são as médias de 3 execuções.

| Resulução | 100x100 | 1000x1000 | 10000x10000 | 20000x20000 |
|-|:-:|:-:|:-:|:-:|
| cells_result_in_memory | 0.095s | 0.342s | 26.912s | 3m59.987s |
| cells_direct_to_file | 0.095s | 0.305s | 18.996s | 2m24.062s |

## Memória necessária


| Resolução | 100x100 | 1000x1000 | 10000x10000 | 20000x20000 |
|-|:-:|:-:|:-:|:-:|
| cells_result_in_memory | - | - | 4GB | 12GB |
| cells_direct_to_file | - | - | - | 4GB |

Atenção: Todos os valores com o valor `-` indicam que não foi necessário forçar o node a alocar mais memória para a sua execução do que a por defeito(1GB para sistemas 64 bit).

## Execução

Para executar as resoluções é necessário executar o comando `node <resolução>.js <teste>.json`.

O resultado irá ser guardado no ficheiro `result.json` na localização onde o comando foi executado.

Nos exemplos onde a memória necessária é especificada o comando a executar deve ser `node --max-old-space-size=<memória em MB> <resolução> <teste>`

### Exemplo
Executar o teste `20000x20000` com a resolução `cells_direct_to_file`.

Comando: `node --max-old-space-size=4096 cells_direct_to_file.js 20000x20000.json`